<?php
require_once 'vendor/autoload.php';
SassCompiler::run("src/scss/", "assets/css/");

// Site variables

$src = 'src/';
$assets = 'assets/';
$pages = 'pages/';
$parts = 'parts/';
$index = 'index.html';

// Image optimizer
//
//$factory = new \ImageOptimizer\OptimizerFactory();
//$optimizer = $factory->get();
//
//$imgsrc = $src.'img/';
//$imgtarget = $assets.'img/';
//
//$optimizer->optimize($filepath);
//optimized file overwrites original one


function checkactive($page = 'index'){
    if($page == $_GET['page']){
        return 'active';
    }
    elseif($page == 'index' AND !isset($_GET['page'])){
        return 'active';
    }
    else{
        return 'inactive';
    }
}

function getpage(){
    if(isset($_GET['page'])){
        return ucfirst($_GET['page']);
    }
    else{
        return 'Etusivu';
    }
}

include $parts.'header.html';

if(isset($_GET['page'])){
    
    $page = $_GET['page'].".html";

    if(file_exists($pages.$page)){
        include $pages.$page;
    }

}
else{
    include $pages.$index;
}

include $parts.'footer.html';

?>