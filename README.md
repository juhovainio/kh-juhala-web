# README #

A pretty static website for KH-Juhala based on Bootstrap. Compiling and package management done with Composer.

## GETTING STARTED ##

Just run "php -S localhost:8000" in the root folder :)

## ADDING PACKAGES ##

1. Install php on your local machine
2. Run "php composer.phar install" in the site root
3. Check for updates with "php composer.phar update"